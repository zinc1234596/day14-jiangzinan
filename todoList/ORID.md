### O:

- Today, we mainly focused on integrating the interaction between the front-end and back-end projects.
- Although I had some prior knowledge about CORS and cross-origin requests, I learned that CORS is not absolutely secure. Even with CORS configured, requests can still enter the program's internal logic, bypassing CORS policies. It also doesn't prevent CSRF . To ensure security, additional mechanisms like validation and authorization are necessary.
- Additionally, I gained a better understanding of the relationship between React, Redux, and React Router. Redux is independent of React, allowing it to be used outside of React applications, but they are often combined together. Similarly, React and React Router are interconnected rather than completely separate entities.
- Through group presentations, I learned about product concepts such as elevator pitches, MVP , user journeys, and user stories. It was fascinating to see how these concepts are derived step by step, and I acquired a lot of new knowledge that I hadn't encountered before.

### R:

- I have mixed feelings today, and I am not deep enough in some product concepts

### I:

- As mentioned earlier, my understanding of cross-origin requests and the React ecosystem is still lacking.
- I need to further study some of the product concepts.

### D:

- Keep my enthusiasm for learning