package oocl.afs.todoList.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "todo")
public class Todo {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Boolean done;
}
