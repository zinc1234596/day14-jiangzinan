package oocl.afs.todoList.advice;

import lombok.Data;

@Data
public class ErrorResponse {
    private Integer code;
    private String message;

    public ErrorResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
