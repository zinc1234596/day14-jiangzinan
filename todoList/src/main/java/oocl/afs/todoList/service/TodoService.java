package oocl.afs.todoList.service;

import exception.TodoNotFoundException;
import oocl.afs.todoList.entity.Todo;
import oocl.afs.todoList.repository.TodoJPARepository;
import oocl.afs.todoList.service.dto.TodoRequest;
import oocl.afs.todoList.service.dto.TodoResponse;
import oocl.afs.todoList.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {
    private final TodoJPARepository todoJPARepository;

    public TodoService(TodoJPARepository todoJPARepository) {
        this.todoJPARepository = todoJPARepository;
    }

    public TodoResponse addTodoItem(TodoRequest request) {
        return TodoMapper.toResponse(todoJPARepository.save(TodoMapper.toEntity(request)));
    }

    public List<TodoResponse> getAllTodos() {
        return TodoMapper.toResponseList(todoJPARepository.findAll());
    }

    public TodoResponse updateTodo(Long id, TodoRequest todoRequest) {
        Todo toBeUpdatedTodo = todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if (todoRequest.getName() != null) {
            toBeUpdatedTodo.setName(todoRequest.getName());
        }
        if (todoRequest.getDone() != null) {
            toBeUpdatedTodo.setDone(todoRequest.getDone());
        }
        return TodoMapper.toResponse(todoJPARepository.save(toBeUpdatedTodo));
    }

    public void deleteTodo(Long id) {
        todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new);
        todoJPARepository.deleteById(id);
    }

    public TodoResponse getTodoById(Long id) {
        return TodoMapper.toResponse(todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }
}
