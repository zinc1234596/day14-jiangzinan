package oocl.afs.todoList.service.dto;

import lombok.Data;

@Data
public class TodoRequest {
    private String name;
    private Boolean done;
}
