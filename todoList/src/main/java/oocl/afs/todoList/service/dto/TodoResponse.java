package oocl.afs.todoList.service.dto;

import lombok.Data;
import oocl.afs.todoList.entity.Todo;

@Data
public class TodoResponse {
    private Long id;

    private String name;

    private Boolean done;

    public TodoResponse(Todo todo) {
    }
}
