package oocl.afs.todoList.service.mapper;

import oocl.afs.todoList.entity.Todo;
import oocl.afs.todoList.service.dto.TodoRequest;
import oocl.afs.todoList.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class TodoMapper {
    public TodoMapper() {
    }

    public static Todo toEntity(TodoRequest request) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(request, todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse(todo);
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static List<TodoResponse> toResponseList(List<Todo> todos) {
        return todos.stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }
}
