package oocl.afs.todoList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import oocl.afs.todoList.entity.Todo;
import oocl.afs.todoList.repository.TodoJPARepository;
import oocl.afs.todoList.service.dto.TodoRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoJPARepository todoJPARepository;

    @BeforeEach
    void setUp() {
        todoJPARepository.deleteAll();
    }

    private static TodoRequest generateTodoRequest() {
        TodoRequest todo = new TodoRequest();
        todo.setName("test todo");
        todo.setDone(false);
        return todo;
    }

    private static Todo generateTodo() {
        Todo todo = new Todo();
        todo.setName("test todo");
        todo.setDone(false);
        return todo;
    }

    @Test
    void should_return_todo_when_create_todo_item_given_request_todo() throws Exception {
        TodoRequest todoItem = generateTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequest = objectMapper.writeValueAsString(todoItem);
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todoItem.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoItem.getDone()));
    }

    @Test
    void should_return_todos_when_findAllTodos() throws Exception {
        Todo todoItem = generateTodo();
        Todo savedTodoItem = todoJPARepository.save(todoItem);
        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(savedTodoItem.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(savedTodoItem.getDone()));
    }

    @Test
    void should_return_updated_todo_item_when_updateTodoItem_given_id_and_request_todo() throws Exception {
        Todo todoItem = generateTodo();
        Todo savedTodoItem = todoJPARepository.save(todoItem);
        TodoRequest todoRequestItem = generateTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(todoRequestItem);
        mockMvc.perform(put("/todo/{id}", savedTodoItem.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(200));
        Optional<Todo> optionalEmployee = todoJPARepository.findById(savedTodoItem.getId());
        assertTrue(optionalEmployee.isPresent());
        Todo updatedTodo = optionalEmployee.get();
        Assertions.assertEquals(todoRequestItem.getName(), updatedTodo.getName());
        Assertions.assertEquals(todoRequestItem.getDone(), updatedTodo.getDone());
        Assertions.assertEquals(savedTodoItem.getId(), updatedTodo.getId());
    }

    @Test
    void should_return_empty_when_deleteTodo_given_id() throws Exception {
        Todo todoItem = generateTodo();
        Todo saveTodoItem = todoJPARepository.save(todoItem);

        mockMvc.perform(delete("/todo/{id}", saveTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        Assertions.assertTrue(todoJPARepository.findById(saveTodoItem.getId()).isEmpty());
    }

    @Test
    void should_return_todo_when_getTodoById_given_id() throws Exception{
        Todo todoItem = generateTodo();
        Todo saveTodoItem = todoJPARepository.save(todoItem);

        mockMvc.perform(get("/todo/{id}", saveTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saveTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(saveTodoItem.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(saveTodoItem.getDone()));
    }
}
